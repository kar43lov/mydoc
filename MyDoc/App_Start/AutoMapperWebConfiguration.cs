﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyDocWeb.ViewModels;
using MyDocBE.Models;
using AutoMapper;

namespace MyDoc.App_Start
{
    public static class AutoMapperWebConfiguration
    {

        public static void Configure()
        {
            ConfigureUserMapping();
        }

        private static void ConfigureUserMapping()
        {
            Mapper.Initialize(cfg =>
                                    {
                                        cfg.CreateMap<UserLoginViewModel, User>();
                                        cfg.CreateMap<User, UserViewModel>();
                                        cfg.CreateMap<Document, DocumentViewModel>().ReverseMap();
                                    });
        }

    }
}