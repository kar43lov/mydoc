﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyDocWeb.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public string FullName { get; set; }
    }
}