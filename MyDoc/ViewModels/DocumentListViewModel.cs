﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyDocWeb.ViewModels
{

    public class DocumentListViewModel
    {
        public List<DocumentViewModel> Documents { get; set; }
        public DocumentSorting Sorting { get; set; }
        public Dictionary<string, string> SortParams { get; set; }
        public string SearchString { get; set; }

        public DocumentListViewModel(List<DocumentViewModel> documents, string searchString, DocumentSorting sorting)
        {
            SearchString = searchString;
            Sorting = sorting;

            SortParams = new Dictionary<string, string>
            {
                {"Name", sorting == DocumentSorting.Name ? DocumentSorting.NameDesc.ToString() : DocumentSorting.Name.ToString()},
                {"FileName", sorting == DocumentSorting.FileName ? DocumentSorting.FileNameDesc.ToString() : DocumentSorting.FileName.ToString()},
                {"CreateDate", sorting == DocumentSorting.CreateDate ? DocumentSorting.CreateDateDesc.ToString() : DocumentSorting.CreateDate.ToString()},
                {"Author", sorting == DocumentSorting.Author ? DocumentSorting.AuthorDesc.ToString() : DocumentSorting.Author.ToString()},
            };

            if (!string.IsNullOrEmpty(searchString))
            {
                documents = documents.Where(x => x.Name.ToLower().Contains(SearchString.ToLower()) ||
                                                 x.FileName.ToLower().Contains(SearchString.ToLower()) ||
                                                 x.CreateDate.ToString().ToLower().Contains(SearchString.ToLower()) ||
                                                 x.AuthorString.ToLower().Contains(SearchString.ToLower())).ToList();
            }

            documents = GetOrderedDocumentList(documents, sorting);

            Documents = documents;
        }

        private static List<DocumentViewModel> GetOrderedDocumentList(List<DocumentViewModel> documents, DocumentSorting sorting)
        {
            switch (sorting)
            {
                case DocumentSorting.Name:
                    documents = documents.OrderBy(x => x.Name).ToList();
                    break;
                case DocumentSorting.NameDesc:
                    documents = documents.OrderByDescending(x => x.Name).ToList();
                    break;
                case DocumentSorting.FileName:
                    documents = documents.OrderBy(x => x.FileName).ToList();
                    break;
                case DocumentSorting.FileNameDesc:
                    documents = documents.OrderByDescending(x => x.FileName).ToList();
                    break;
                case DocumentSorting.CreateDate:
                    documents = documents.OrderBy(x => x.CreateDate).ToList();
                    break;
                case DocumentSorting.CreateDateDesc:
                    documents = documents.OrderByDescending(x => x.CreateDate).ToList();
                    break;
                case DocumentSorting.Author:
                    documents = documents.OrderBy(x => x.AuthorString).ToList();
                    break;
                case DocumentSorting.AuthorDesc:
                    documents = documents.OrderByDescending(x => x.AuthorString).ToList();
                    break;
                case DocumentSorting.Default:
                    documents = documents.OrderBy(x => x.CreateDate).ThenBy(x => x.Name).ToList();
                    break;
            }
            return documents;
        }

    }

    public enum DocumentSorting
    {
        Default,
        Name,
        NameDesc,
        FileName,
        FileNameDesc,
        CreateDate,
        CreateDateDesc,
        Author,
        AuthorDesc
    }
}