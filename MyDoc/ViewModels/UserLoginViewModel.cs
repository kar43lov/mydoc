﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyDocWeb.ViewModels
{
    public class UserLoginViewModel
    {
        [Required (ErrorMessage="Поле должно быть заполнено")]
        [Display(Name="Логин: ")]
        public string Login { get; set; } 

        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [Display(Name = "Пароль: ")]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }
}