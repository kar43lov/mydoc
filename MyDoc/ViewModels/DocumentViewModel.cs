﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MyDocBE.Models;

namespace MyDocWeb.ViewModels
{
    public class DocumentViewModel
    {
        [Required(ErrorMessage = "Поле должно быть заполнено")]
        [StringLength(80, ErrorMessage = "Название документа должно быть не более 80 символов")]
        [Display(Name = "Название документа: ")]
        public string Name { get; set; }

        public DateTime CreateDate { get; set; }

        public string AuthorString { get; set; }

        public User Author { get; set; }

        public string FileName { get; set; }

        public Guid FileGuid { get; set; }
    }
}