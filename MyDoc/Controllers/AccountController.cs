﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using AutoMapper;

using MyDocWeb.ViewModels;
using MyDocBE.Models;
using MyDocDAL;
using MyDocDAL.Interfaces;

namespace MyDocWeb.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {

        IUserRepository userrepository;

        public AccountController(IUserRepository r)
        {
            userrepository = r;
        }

        public ActionResult Login(string returnUrl = null)
        {
            return View(new UserLoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        public ActionResult Login(UserLoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = Mapper.Map<User, UserViewModel>(userrepository.Login(model.Login, model.Password));

                if (user != null)
                {
                    Session["Id"] = user.Id.ToString();
                    Session["Login"] = user.Login;
                    Session["FullName"] = user.FullName;
                    FormsAuthentication.SetAuthCookie(user.Id.ToString(), false);
                    
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                    {
                        return Redirect(model.ReturnUrl);
                    }
                    return RedirectToAction("Index", "Documents");   
                }

                ModelState.AddModelError("", "Неверный логин или пароль");
            }
            return View(model);
        }


        public ActionResult LogOut()
        {
            Session["Id"] = null;
            Session["Login"] = null;
            Session["FullName"] = null;
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

    }
}
