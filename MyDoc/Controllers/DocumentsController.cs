﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.IO;

using AutoMapper;

using MyDocWeb.ViewModels;
using MyDocBE.Models;
using MyDocDAL;
using MyDocDAL.Interfaces;


namespace MyDocWeb.Controllers
{
    [Authorize]
    public class DocumentsController : Controller
    {

        IDocumentRepository idocumentrepository;
        public DocumentsController(IDocumentRepository r)
        {
            idocumentrepository = r;
        }

        public ActionResult Index(DocumentSorting sorting = DocumentSorting.Default, string searchString = "")
        {
            var documents = Mapper.Map<List<Document>, List<DocumentViewModel>>(idocumentrepository.GetDocuments());
            foreach (var d in documents)
            {
                d.AuthorString = d.Author.FullName;
            }
            var model = new DocumentListViewModel(documents, searchString, sorting);
            ViewBag.FullName = Session["FullName"];
            return View(model);
        }

        public ActionResult Add()
        {
            ViewBag.FullName = Session["FullName"];
            return View();
        }

        [HttpPost]
        public ActionResult Add(DocumentViewModel model, HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null)
                {
                    model.FileName = System.IO.Path.GetFileName(upload.FileName);
                    if (model.FileName.Length > 70) model.FileName = model.FileName.Substring(0, 70) + "..." + model.FileName.Substring(model.FileName.Length - 7, 7);
                    model.FileGuid = Guid.NewGuid();
                    model.CreateDate = DateTime.Now;
                    model.Author = new User();
                    model.Author.Id = Int32.Parse(FormsAuthentication
                        .Decrypt(HttpContext.Request.Cookies.Get(FormsAuthentication.FormsCookieName).Value).Name);
                     
                    idocumentrepository.InsertDocument(Mapper.Map<DocumentViewModel, Document>(model));
                    upload.SaveAs(Server.MapPath("~/Resources/files/" + model.FileGuid.ToString()));
                    return RedirectToAction("Index", "Documents");
                }
                ModelState.AddModelError("", "Необходимо выбрать документ");
                return View();
            }
            return View();
        }

        public ActionResult Download(Guid fileGuid)
        {
            if (System.IO.File.Exists(Server.MapPath("~/Resources/files/" + fileGuid.ToString())))
            {
                return GetFile(Mapper.Map<Document, DocumentViewModel>(idocumentrepository.GetDocumentByGuid(fileGuid)));
            }
            TempData["errorMessage"] = "Данный файл не найден.";
            TempData.Keep("errorMessage");
            return RedirectToAction("Index", "Documents"); 
        }

        public FileResult GetFile(DocumentViewModel model)
        {
            string file_path = Server.MapPath("~/Resources/files/" + model.FileGuid.ToString());
            string file_type = "application/pdf";
            string file_name = model.FileName;
            return File(file_path, file_type, file_name);
        }

    }
}
