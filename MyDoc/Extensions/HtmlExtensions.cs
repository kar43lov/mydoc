﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MyDocWeb.Extensions
{
    public static class HtmlExtensions
    {

        public static IHtmlString SortIdentifier(this HtmlHelper htmlHelper, string sortOrder, string field)
        {
            if (string.IsNullOrEmpty(sortOrder) ||
                (sortOrder.Trim() != field && sortOrder.Replace("Desc", "").Trim() != field)) return null;

            var glyph = "glyphicon glyphicon-triangle-top";
            if (sortOrder.Contains("Desc"))
            {
                glyph = "glyphicon glyphicon-triangle-bottom";
            }

            var span = new TagBuilder("span");
            span.Attributes["class"] = glyph;
            span.Attributes["style"] = "font-size: 9pt; color: #337ab7;";

            return MvcHtmlString.Create(span.ToString());
        }

        public static RouteValueDictionary ToRouteValueDictionary(this NameValueCollection collection, string newKey, string newValue)
        {
            var routeValueDictionary = new RouteValueDictionary();

            foreach (var key in collection.AllKeys.Where(key => key != null))
            {
                if (routeValueDictionary.ContainsKey(key))
                    routeValueDictionary.Remove(key);

                routeValueDictionary.Add(key, collection[key]);
            }

            if (string.IsNullOrEmpty(newValue))
            {
                routeValueDictionary.Remove(newKey);
            }
            else
            {
                if (routeValueDictionary.ContainsKey(newKey))
                    routeValueDictionary.Remove(newKey);

                routeValueDictionary.Add(newKey, newValue);
            }

            return routeValueDictionary;
        }

    }
}