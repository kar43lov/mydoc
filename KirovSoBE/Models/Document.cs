﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDocBE.Models
{
    public class Document
    {     
        public virtual int Id { get; set; }
        
        public virtual string Name { get; set; }
        
        public virtual DateTime CreateDate { get; set; }
              
        public virtual string FileName { get; set; }
        
        public virtual Guid FileGuid { get; set; }

        public virtual User Author { get; set; }
    }
}