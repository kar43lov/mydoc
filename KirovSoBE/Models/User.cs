﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyDocBE.Models
{
    public class User
    {
        private ICollection<Document> _document;

        public virtual int Id { get; set;}
        
        public virtual string Login { get; set; } 
        
        public virtual byte[] Password { get; set; }
        
        public virtual string Salt { get; set; }
        
        public virtual string FullName { get; set; }

        public virtual ICollection<Document> Document
        {
            get { return this._document; }
            set { this._document = value; }
        }
    }
}