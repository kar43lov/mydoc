﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyDocDAL.Interfaces;
using MyDocBE.Models;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Transform;

namespace MyDocDAL
{
    public class DocumentRepository : IDocumentRepository
    {

        public List<Document> GetDocuments()
        {
            using (ISession session = HibernateHelper.OpenSession())
            {
                var documents = session.QueryOver<Document>().Fetch(x => x.Author).Eager.Future().ToList();
                return documents ?? null;
            }
        }

        public void InsertDocument(Document model)
        {
            using (ISession session = HibernateHelper.OpenSession())
            {
                session
                .GetNamedQuery("sp_InsertDocument")
                .SetParameter("name", model.Name)
                .SetParameter("createdate", model.CreateDate)
                .SetParameter("authorid", model.Author.Id)
                .SetParameter("filename", model.FileName)
                .SetParameter("fileguid", model.FileGuid)
                .ExecuteUpdate();
            }
        }

        public Document GetDocumentByGuid(Guid fileguid)
        {
            using (ISession session = HibernateHelper.OpenSession())
            {
                var document = session.QueryOver<Document>().Where(x => x.FileGuid == fileguid).SingleOrDefault();
                return document ?? null;
            }
        }

    }
}