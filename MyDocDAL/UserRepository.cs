﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyDocDAL.Interfaces;
using MyDocBE.Models;

using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Transform;

namespace MyDocDAL
{
    public class UserRepository : IUserRepository
    {

        public User Login(String login, String password)
        {
            User user;
            using (ISession session = HibernateHelper.OpenSession())
            {
                user = session
                    .GetNamedQuery("sp_CheckUserLoginPas")
                    .SetParameter("login", login)
                    .SetParameter("password", password)
                    .SetResultTransformer(Transformers.AliasToBean(typeof(User)))
                    .UniqueResult<User>();
            }
            return user ?? null;
        }

    }
}