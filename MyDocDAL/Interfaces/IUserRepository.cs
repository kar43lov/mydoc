﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MyDocBE.Models;

namespace MyDocDAL.Interfaces
{
    public interface IUserRepository
    {
        User Login(String login, String password);
    }
}