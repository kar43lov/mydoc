CREATE DATABASE MyDOC
GO

use MyDOC

CREATE TABLE Users(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[Login] NVARCHAR(64) NULL UNIQUE,
	[Password] varbinary(20) NULL,
	[Salt] char(25),		
	[FullName] NVARCHAR(64) NOT NULL
);
GO

CREATE TABLE Documents(
[Id] INT NOT NULL PRIMARY KEY IDENTITY,
[Name] nvarCHAR(80) NOT NULL,
[CreateDate] datetime NOT NULL,
[AuthorId] INT NOT NULL,
[FileName] nvarCHAR(80) NOT NULL,
[FileGuid] uniqueidentifier  NOT NULL
);
GO

ALTER TABLE Documents
ADD FOREIGN KEY (AuthorId)
REFERENCES Users
GO

CREATE PROCEDURE [dbo].[sp_CreateUser]
	@login NVARCHAR(64),
	@password NVARCHAR(64),
	@fullName NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	declare @salt VARCHAR(25);
	declare @pwdWithSalt VARCHAR(125);
	
	DECLARE @Seed int;
	DECLARE @LCV tinyint;
	DECLARE @CTime DATETIME;

	SET @CTime = GETDATE();
	SET @Seed = (DATEPART(hh, @Ctime) * 10000000) + (DATEPART(n, @CTime) * 100000)
	    + (DATEPART(s, @CTime) * 1000) + DATEPART(ms, @CTime);
	SET @LCV = 1;
	SET @salt = CHAR(ROUND((RAND(@Seed) * 94.0) + 32, 3));

	WHILE (@LCV < 25)
	BEGIN
	  SET @salt = @salt + CHAR(ROUND((RAND() * 94.0) + 32, 3));
	SET @LCV = @LCV + 1;
	END;

	SET @pwdWithSalt = @salt + @password;
	declare @passwordHash varbinary(20) = hashbytes('sha1', cast(@pwdWithSalt as varbinary(128)))
	
	INSERT INTO [dbo].[Users] ([Login], [Password], [Salt], [FullName])
	VALUES(@login, @passwordHash, @salt, @fullName)
END
GO

exec sp_CreateUser @login=N'kar43lov', @password=N'123', @fullName=N'������ ����� �����������'
exec sp_CreateUser @login=N'qwe', @password=N'asd', @fullName=N'������� ��������� ���������'
exec sp_CreateUser @login=N'123', @password=N'123', @fullName=N'������� ���� �������������'
GO


CREATE PROCEDURE [dbo].[sp_CheckUserLoginPas]
	@login NVARCHAR(64),
	@password NVARCHAR(64)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Salt CHAR(25);
	DECLARE @PwdWithSalt VARCHAR(125);
	DECLARE @PwdHash VARBINARY(20);  

	SELECT @Salt = Salt, @PwdHash = Password 
	FROM users WHERE @login = Login;
	
	SET @PwdWithSalt = @Salt + @password;

	SELECT top 1
		*	
	FROM [Users] 
	WHERE Login = @login and Password = HASHBYTES('sha1', CAST(@PwdWithSalt AS VARBINARY(128)))
END
GO

CREATE PROCEDURE [dbo].[sp_InsertDocument]
    @name NVARCHAR(80),
	@createdate DATETIME,
	@authorid INT,
	@filename NVARCHAR(80),
	@fileguid uniqueidentifier
AS
	INSERT INTO Documents (Name,CreateDate,AuthorId,FileName,FileGuid)
	VALUES (@name, @createdate, @authorid, @filename, @fileguid)
  
    SELECT SCOPE_IDENTITY()
Go